FROM python:3.6-buster

WORKDIR /usr/src/app

#USER 1000900000:1000900000

COPY requirements.txt ./

#USER 0:0

#### turicreate

RUN apt-get install -y libstdc++6; easy_install pip; pip install --upgrade pip; pip install --no-cache-dir -r requirements.txt

#RUN apt-get install -y libstdc++6
#RUN easy_install pip
#RUN pip install --upgrade pip
#RUN pip install turicreate==5.7
#RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8080

COPY . .

RUN chown -R 1000900000:1000900000 .

CMD ["python", "./app/app.py"]