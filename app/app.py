from flask import Flask
from logging import FileHandler, WARNING

app = Flask(__name__)
app.config.from_pyfile('config.py')

file_handler = FileHandler('log.txt')
file_handler.setLevel(WARNING)
app.logger.addHandler(file_handler)

from views import *

if __name__ == '__main__':
    print('''

                                                                    
             88                                                             
             88                                                             
             88                                                             
     ,adPPYb,88  ,adPPYba,   ,adPPYba,  8b,dPPYba,   ,adPPYba,   ,adPPYba,  
    a8"    `Y88  I8[    ""  a8P_____88  88P'   `"8a  I8[    ""  a8P_____88  
    8b       88   `"Y8ba,   8PP"""""""  88       88   `"Y8ba,   8PP"""""""  
    "8a,   ,d88  aa    ]8I  "8b,   ,aa  88       88  aa    ]8I  "8b,   ,aa  
     `"8bbdP"Y8  `"YbbdP"'   `"Ybbd8"'  88       88  `"YbbdP"'   `"Ybbd8"'                                                                 
                                                            
                                        

    ''')
    app.run(host='0.0.0.0', port=8080)