from flask import request, jsonify
import turicreate as tc
from app import app
import numpy as np
import cv2 as cv
import os


def decode_image(image_bytes):
    return cv.imdecode(np.frombuffer(image_bytes, np.uint8), -1)

@app.route('/', methods=['GET'])
def index():
    if request.method == 'GET':
        return jsonify({ 'message':'dSense'})

@app.route('/classify', methods=['POST'])
def identify():
    if 'image' not in request.files:
        return jsonify({'message':'no image found'}), 400
    if request.method == 'POST':
        model = tc.load_model(os.path.join(os.getcwd(), 'app/model'))

        image_bytes = request.files['image']
        image = decode_image(image_bytes.read())

        cv.imwrite(os.path.join(os.getcwd(), 'app/_temp.png'), image)

        test = tc.SFrame({'image':[tc.Image(os.path.join(os.getcwd(), 'app/_temp.png'))]})
        predictions = model.predict_topk(test, k=5)
    else:
        return jsonify({ 'message':'error'}), 400
    return jsonify([predictions[result] for result in range(len(predictions))]), 200
